# river-gruvbox-rice

## Preview

![Bookmarks](previews/bookmarks.png)
![Emacs](previews/emacs.png)
![pcmanfm](previews/pcmanfm.png)
![live-bg](previews/live-bg.png)
![librewolf](previews/librewolf.png)
![scriptlauncher](previews/scriptlauncher.png)
![Htop](previews/htop.png)

## Dependencies

Fonts
```
Fantasque Sans Mono, Go-Mono, JetBrains Mono, Cascadia Code
```

Applications
```
pcmanfm, Librewolf, yambar, fastfetch, river, bemenu, foot, fnott, emacs, zathura, swaybg, mpvpaper, swaylock, mksh, bash
```

## Work in progress
This project is still unfinished; some themes require updating and many scripts are written in a suboptimal way and include hardcoded paths which aren't documented yet. There are some things planned in addition to cleaning up:

- Adding links to used themes and resources
- Documenting niche functionality
- Providing a script to download all wallpapers from their original source instead of hosting them here because of copyright issues
- Providing an installation script
- Providing a command to install all required packages on all supported distros 

### Shortcomings
To make the colorscheme of bemenu more interchangeable, most scripts now rely on bash. It it possible to hardcode the colors if you can't or don't want to install bash.

### Contributing
If you want to contribute or have a feature you want to request, just create a feature/pull request. I don't mind making significant changes to poorly written scripts and I would be glad to add more scripts to the collection. Wallpaper contributions are also always welcome.

### Color tables

| Dark    | Codes  |
|---------|--------|
| black   | 32302f |
| red     | cc241d |
| green   | 98971a |
| yellow  | d79921 |
| blue    | 458588 |
| magenta | b16286 |
| cyan    | 689d6a |
| white   | f2e5bc |

| Bright  | Codes  |
|---------|--------|
| black   | 32302f |
| red     | fb4934 |
| green   | b8bb26 |
| yellow  | fabd2f |
| blue    | 83a598 |
| magenta | d3869b |
| cyan    | 8ec07c |
| white   | f2e5bc |

### This project is making use of or inspired by:

-[eastack's gruvbox theme for zathura](https://github.com/eastack/zathura-gruvbox)

-[Fausto-Korpvart's gruvbox GTK theme](https://github.com/Fausto-Korpsvart/Gruvbox-GTK-Theme)

-[varlesh's volantes-cursors](https://github.com/varlesh/volantes-cursors)

-[Henrik Lissner's Doom Emacs](https://github.com/doomemacs/doomemacs)

-[Luke Smith's bookmarks script](https://www.youtube.com/watch?v=d_11QaTlf1I).

## Installation

==**It is in all cases advised to update the system before proceeding with the installation.**==
### Arch 
WIP
```
-S emacs river pcmanfm mksh bash zathura zathura-pdf-poppler foot fnott yambar-wayland fastfetch librewolf-bin ttf-fantasque-sans-git 
```
Note that not all required packages are available in the official repos, so you have to use your AUR helper of choice or install the packages manually.
For faster script execution, install ```pacman -S dash``` [and link it to /bin/sh](https://wiki.archlinux.org/title/Dash).

### Debian
WIP

First update your system.
```
```



### Gentoo

First add the required overlays.
```
eselect repository enable guru wayland-desktop librewolf && emaint sync -r guru wayland-desktop librewolf
```

```
emerge --noreplace gui-wm/river x11-misc/pcmanfm app-shells/dash app-shells/mksh app-shells/bash app-text/zathura app-text/zathura-pdf-poppler gui-apps/foot gui-apps/fnott gui-apps/yambar app-misc/fastfetch www-client/librewolf media-fonts/fantasque-sans-mono gui-apps/swaylock gui-apps/mpvpaper dev-libs/bemenu gui-apps/swaybg media-fonts/gofont-mono media-fonts/cascadia-code media-fonts/jetbrains-mono gui-apps/wtype media-fonts/noto-emoji
```
Note that there is a binary package for librewolf; it is called ```librewolf-bin```.
To speed up script execution, link dash to your /bin/sh: ```ln -sfT dash /usr/bin/sh```.

You will have to [unmask](https://wiki.gentoo.org/wiki/Knowledge_Base:Unmasking_a_package) some packages and dependencies. Also consider setting the wanted USE flags prior to the installation to avoid a lengthy recompilation, especially in the case of Librewolf.

### Fedora
WIP
```

```


### OpenSUSE
WIP
```

```
WIP
### Void
```

```

## FAQ
**I own one or more of the hosted wallpapers and want it/them removed.**

You can send me an email at **ThomasW2003@Tutanota.de** or create a bug report. I won't ask for any evidence as it is just not worth the hassle for me.

**Why is foobar not present in the bookmarks file?**

I only added bookmarks that I would personally use. You can add your own bookmarks easily, I just wanted to demonstrate what sort of stuff you can put in there because it can be used for way more than just URLs. I also am not affiliated with any of the websites. 

**Why is my Distro not (yet) supported?**

Some Distributions don't ship all required packages. You might be able to install them from third party repos or build them yourself, but documenting this is outside the scope of this project. The configuration files should all be distribution agnostic as the functionality of all used applications is fairly stable. I might add a guide to install the required packages via the nix package manager in the future.

**Why is foo not in the bar directory?**

There are 2 reasons. First of all, I don't want to create any conflicts with already existing files on users system & second I think that the file system in Linux is rather messy and doesn't provide a directory where such files can be stored.

**Why does the bookmarks selector cover my entire screen or leave a margin at the bottom?**

The selector is tested to cover every pixel on a 1920x1080 display. If you wish to not have your entire screen covered, decrease the amount specified at the ```-l``` flag in the bookmarks script. If the selector leaves a small margin at the bottom of your screen, try either changing the font size and/or increasing the aforementioned value.
