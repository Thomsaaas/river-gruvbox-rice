(setq user-full-name ""
      user-mail-address "")

(setq org-directory "~/Documents/org/")

(setq doom-theme 'doom-gruvbox)

(setq display-line-numbers-type t)

(setq doom-font (font-spec :family "Fantasque Sans Mono" :size 19 :weight 'medium)
      doom-variable-pitch-font (font-spec :family "Fantasque Sans Mono" :size 19))

(setq initial-buffer-choice (lambda () (get-buffer-create "*dashboard*")))

(display-time-mode t)
(setq display-time-format "%a, %d.%m. | %R ")
(setq display-time-default-load-average nil)
(setq display-time-24hr-format t)
(setq confirm-kill-emacs nil)
(setq gc-cons-threshold 131072)
(display-battery-mode 1)


(add-hook 'olivetti-mode-hook (lambda () (display-line-numbers-mode 'toggle)))
(add-hook 'olivetti-mode-hook (lambda () (text-scale-increase 3)))

(add-hook 'normal-mode-hook (lambda () (display-line-numbers-mode 'toggle)))
(add-hook 'normal-mode-hook (lambda () (text-scale-decrease 3)))


;; Org-bullets
(add-hook 'org-mode-hook (lambda) () (org-bullets-mode 1))

;; Opacity
;;(set-frame-parameter (selected-frame) 'alpha '(80 . 80))
;;(add-to-list 'default-frame-alist '(alpha . (80 . 80)))

(use-package dashboard
 ; :ensure t
  :config
(dashboard-setup-startup-hook)
(setq dashboard-startup-banner "~/.config/emacs/img/startup-banner.png")
(setq dashboard-items '((recents  . 5)
                        (bookmarks . 5)
                        (projects . 5)))
(setq dashboard-show-shortcuts nil)
(setq dashboard-center-content nil)
(setq dashboard-set-file-icons t)
(setq dashboard-set-heading-icons t)
(setq dashboard-banner-logo-title "Vi, Vi, Vi is the editor of the beast."))

(setq dashboard-footer-icon (all-the-icons-octicon "dashboard"
                                                   :height 1.1
                                                   :v-adjust -0.05
                                                   :face 'font-lock-keyword-face))
;; keyboard-map


;; increase text-scale
(map! :leader
      :desc "IncFS"
      "g f" #'text-scale-increase)


;; decrease text-scale
(map! :leader
      :desc "DecFS"
      "d f" #'text-scale-decrease)

;; load theme
(map! :leader
      :desc "Load-Theme"
     "t t" #'load-theme)

;; open file as root
(map! :leader
      :desc "Edit as root"
      "s d" #'doom/sudo-this-file)

;; toggle line numbers
(map! :leader
      :desc "Toggle line numbers"
      "l v" #'display-line-numbers-mode)

;; toggle mode to olivetti
(map! :leader
      :desc "Focus"
      "f c" #'olivetti-mode)

;; toggle mode to normal
(map! :leader
      :desc "Normal"
      "f v" #'normal-mode)

;; magit
(map! :leader
      :desc "Magit"
      "g g" #'magit)

;; magit push
(map! :leader
      :desc "Magit"
      "g p" #'magit-push)

;; magit push
(map! :leader
      :desc "Magit"
      "g c" #'magit-commit)

;; open manpage
(map! :leader
      :desc "Woman"
      "w w" #'woman)
